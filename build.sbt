name := "toy-thermostate-sbt"

version := "0.1"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"   % "2.4.20",
  "com.typesafe.akka" %% "akka-slf4j"   % "2.4.20",
  "com.typesafe.akka" %% "akka-remote"  % "2.4.20",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.20" % "test",
  "org.scalaj" %% "scalaj-http" % "2.4.2",
  "org.scala-lang.modules" %% "scala-swing" % "2.1.1",
  "io.vertx" %% "vertx-lang-scala" % "3.5.4",
  "io.vertx" %% "vertx-web-scala" % "3.5.4"
)