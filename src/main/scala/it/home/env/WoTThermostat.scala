package it.home.env

import java.util.concurrent.Executors

import io.vertx.lang.scala.json.Json
import io.vertx.scala.core.Vertx
import it.agent.wot.{DigitalEnv, Resource}
import it.twin.Thermostat

import scala.concurrent.{Await, ExecutionContext, Future, Promise}

trait WoTThermostat extends Resource[WoTThermostat] with Thermostat { }

object WoTThermostat {
  def from(env : DigitalEnv, thermostatName : String): WoTThermostat = {
    env.resource(thermostatName) match {
      case Some(v : WoTThermostat) => v
      case _ => throw new IllegalArgumentException("env doesn't has thermo")
    }
  }

  def apply(name : String,
            port : Int,
            envUrl : String): WoTThermostat = {
    new WoTThermostatImpl(name, port = port, envUrl = envUrl)
  }

  import Thermostat._

  import scala.concurrent.duration._
  private implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())
  private implicit val timeout: Duration = 5 second
  private val vertx = Vertx.vertx()
  private class WoTThermostatImpl(override val name : String,
                                  override val temp : Double = 0,
                                  override val state : State = Stop,
                                  port : Int,
                                  envUrl : String) extends WoTThermostat {
    val client = vertx.createHttpClient()
    override def sense(): Future[WoTThermostat] = {
      val tempPromise = Promise.apply[Double]()
      val statePromise = Promise.apply[State]()
      println(name)

      client.getNow(port, envUrl, name + "/properties/temperature",
        responseHandler = response => response.bodyHandler(body => {
            val response = Json.fromObjectString(body.toString())
            tempPromise.success(response.getDouble("temperature"))
          })
      )
      client.getNow(port, envUrl, name + "/properties/state",
        response => response.bodyHandler(body => {
          val response = Json.fromObjectString(body.toString())
          statePromise.success(State(response.getString("state")))
        })
      )
      concurrent.Future {
        val temp = Await.result(tempPromise.future, timeout)
        val state = Await.result(statePromise.future, timeout)
        new WoTThermostatImpl(name, temp, state, port, envUrl)
      }
    }

    private def doPostOn(method : String): Unit = {
      val endPost = Promise.apply[Int]()
      client.post(port, envUrl, name +  method)
        .handler(end => endPost.success(end.statusCode()))
          .putHeader("Content-Length", "0")
          .write("")
      Await.result(endPost.future, timeout)
    }

    override def hotter: Unit = doPostOn("/actions/startHeating")

    override def colder: Unit = doPostOn("/actions/startCooling")

    override def stop: Unit =  doPostOn("/actions/stop")
  }
}
