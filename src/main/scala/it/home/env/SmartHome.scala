package it.home.env

import java.util.concurrent.Executors

import akka.util.Timeout
import it.agent.wot.{DigitalEnv, Resource}

import scala.concurrent.{ExecutionContext, Future}

class SmartHome(private var smartThermostat: WoTThermostat) extends DigitalEnv {
  import scala.concurrent.duration._
  implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())
  implicit val timeout: Timeout = 5 second

  override def sense: Future[DigitalEnv] = {
    smartThermostat.sense.map(thermostat => new SmartHome(thermostat))
  }

  override def resource(name: String): Option[Resource[_]] = if(name == smartThermostat.name) {
    Some(smartThermostat)
  } else {
    None
  }

  override def toString = s"SmartHome($smartThermostat)"
}
