package it.home

import it.home.protocol.ChangeTempMsg

import scala.collection.mutable

object Study3 extends App {
  import Setup._
  import it.agent.wot.Plan._
  import it.agent.wot.WoTSpace.dsl._

  create agent {
    create context (
      name = agentName,
      environment = env,
      state = mutable.Map[String, Any](tempName -> initTemp)
    )
  } goal {
    create plan (
      maintain(goal, action, onSatisfied),
      onMessage(ChangeTempMsg.header, (context, msg) => msg match {
        case tempMsg: Double =>
          context.state(tempName) = tempMsg
        case _ =>
      })
    )
  }
}
