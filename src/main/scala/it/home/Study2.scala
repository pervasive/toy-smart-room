package it.home

import scala.collection.mutable

object Study2 extends App {
  import Setup._
  import it.agent.wot.Plan._
  import it.agent.wot.WoTSpace.dsl._

  create agent {
    create context (
      name = agentName,
      environment = env,
      state = mutable.Map[String, Any](tempName -> initTemp)
    )
  } goal {
    create plan maintain(goal, action, onSatisfied)
  }
}
