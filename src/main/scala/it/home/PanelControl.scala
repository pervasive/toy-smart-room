package it.home

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{BorderLayout, Dimension}

import it.agent.wot.{DigitalEnv, WoTSpace}
import it.home.Setup.resourceName
import it.home.env.WoTThermostat
import it.home.protocol.ChangeTempMsg
import javax.swing._

import scala.collection.mutable

class PanelControl(env : DigitalEnv,
                   agentName : String,
                   minTemp : Int,
                   maxTemp : Int,
                   temp : Int) {
  private val COLUMN = 12
  import it.agent.wot.Plan._
  import it.agent.wot.WoTSpace.dsl._
  def show() : Unit = {
    val temperatureText = new JLabel("Temp : ")
    val stateText = new JLabel("State : ")
    val limitTemp = new JTextField(temp.toString)
    limitTemp.setColumns(COLUMN)
    val limitTempButton = new JButton("Change")
    val limitTempLabel = new JLabel("change temp:")

    limitTempButton.addActionListener((e: ActionEvent) => {
      WoTSpace.medium.sendMessageTo(new ChangeTempMsg(limitTemp.getText().toDouble), agentName)
    })
    create agent {
      create context (
        name = "guiAgent",
        environment = env,
        state = mutable.Map[String, Any]()
      )
    } goal {
      create plan forever {
        context => {
          val thermostat = WoTThermostat.from(context.environment, resourceName)
          temperatureText.setText("Temp = " + thermostat.temp)
          stateText.setText("State = " + thermostat.state)
        }
      }
    }
    val box = Box.createVerticalBox()
    box.add(temperatureText)
    box.add(stateText)

    val tempChange = new JPanel()
    tempChange.add(limitTempLabel)
    tempChange.add(limitTemp)
    tempChange.add(limitTempButton)
    box.add(tempChange)

    val frame = new JFrame("Termostat")
    frame.getContentPane.add(box, BorderLayout.CENTER)
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    frame.setSize(new Dimension(600, 400))
    frame.setLocationRelativeTo(null)
    frame.setVisible(true)
  }
}
