package it.home.protocol

import it.agent.medium.Message

/**
  * a message sent to my "smart" thermostat to
  * change the reference temperature
  * @param temp the new target temp
  */
class ChangeTempMsg(temp : Double) extends Message{
  override def header: String = "ChangeTempMsg"

  override def payload: Any = temp
}

object ChangeTempMsg {
  def header = "ChangeTempMsg"
}