package it.home

import it.home.env.{SmartHome, WoTThermostat}
import it.twin.Thermostat.{Cold, Hot, Stop}

object Setup {
  import it.agent.wot.WoTSpace.Context
  val resourceName = "/thermostat"
  val tempName = "temp"
  val env = new SmartHome(WoTThermostat(resourceName, 8080, "localhost"))
  val threshold = 0.5
  val initTemp = 15.0
  val agentName = "thermostatAgent"
  val minTemp = -40
  val maxTemp = 50

  def retrieveTempAndThermostat(context: Context) = {
    val temp = context.state(tempName).asInstanceOf[Double]
    val thermostat = WoTThermostat.from(context.environment, resourceName)
    (temp, thermostat)
  }

  def onSatisfied(context : Context) : Unit = {
    val (_, term) = retrieveTempAndThermostat(context)
    if(term.state != Stop) {
      println("on goal satisfied")
      term.stop
    }
  }

  def goal(context : Context) : Boolean = {
    val (desiredTemp, term) = retrieveTempAndThermostat(context)
    println("check goal: " + (term.temp - desiredTemp))
    Math.abs(term.temp - desiredTemp) < threshold
  }

  def action(context: Context) : Unit = {
    val (desiredTemp, thermostat) = retrieveTempAndThermostat(context)
    println("goal value :"  + desiredTemp + " sensed : " + thermostat.temp)
    if(desiredTemp > thermostat.temp && thermostat.state != Hot) {
      thermostat.hotter
    } else if (desiredTemp < thermostat.temp && thermostat.state != Cold) {
      thermostat.colder
    }
  }

  new PanelControl(env, agentName, minTemp, maxTemp, initTemp.toInt).show()
}
