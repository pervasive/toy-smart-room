package it.twin

import java.util.concurrent.{Executors, TimeUnit}

import scala.concurrent.{Await, ExecutionContext, Future}

class ThermostatSimulation(private var innerTemp : Double,
                           private val deltaTemp : Double,
                           private val period: Int) extends Thermostat {
  import Thermostat._

  import scala.concurrent.duration._
  private val scheduled = Executors.newSingleThreadScheduledExecutor()
  private implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(scheduled)
  private implicit val timeout: Duration = 5 second
  private var innerState : State = Stop
  private val thr = 0.1

  private val logic : Runnable = () => {
    innerState match {
      case Hot => innerTemp += randomizeNearValue(deltaTemp)
      case Cold => innerTemp -= randomizeNearValue(deltaTemp)
      case _ => innerTemp -= randomizeNearValue(envFactor)
    }
  }
  private def envFactor = deltaTemp / 100

  private def randomizeNearValue(value : Double) : Double = {
    value + (value * Math.random())
  }
  def start(): ThermostatSimulation = {
    scheduled.scheduleAtFixedRate(logic, 0, 100, TimeUnit.MILLISECONDS)
    this
  }

  def temp : Double = Await.result(Future {
    innerTemp
  }, timeout)

  def state : State = Await.result(Future {
    innerState
  }, timeout)

  private def setState(state: State) : Future[Unit] = Future {
    innerState = state
  }

  override def hotter: Unit = Await.result(setState(Hot), timeout)

  override def colder: Unit = Await.result(setState(Cold), timeout)

  override def stop: Unit = Await.result(setState(Stop), timeout)
}
