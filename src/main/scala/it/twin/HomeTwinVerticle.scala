package it.home.env

import io.vertx.lang.scala.ScalaVerticle
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.Router
import it.twin.Thermostat

import scala.concurrent.Future

class HomeTwinVerticle(port : Int, thermostast: Thermostat) extends ScalaVerticle {
  val baseUrl = "/thermostat"
  private def temp : String = Json.obj("temperature" -> thermostast.temp).encode()
  private def state : String = Json.obj("state" -> thermostast.state.toString).encode()
  override def startFuture(): Future[Unit] = {
    val router = Router.router(vertx)
    router
      .get(s"$baseUrl/properties/temperature")
      .handler(_.response().end(temp))

    router
      .get(s"$baseUrl/properties/state")
      .handler(_.response().end(state))

    router
      .post(s"$baseUrl/actions/startHeating")
      .handler(request => {
        thermostast.hotter()
        request.response().end()
      })

    router
      .post(s"$baseUrl/actions/startCooling")
      .handler(request => {
        thermostast.colder()
        request.response().end()
      })

    router
      .post(s"$baseUrl/actions/stop")
      .handler(request => {
        thermostast.stop()
        request.response().end()
      })

    vertx
      .createHttpServer()
      .requestHandler(router.accept)
      .listenFuture(port, "0.0.0.0")
      .map(_ => ())
  }
}