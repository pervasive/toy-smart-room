package it.twin

trait Thermostat {
  import Thermostat._
  def temp : Double

  def hotter() : Unit

  def colder() : Unit

  def stop() : Unit

  def state : State
}
object Thermostat {
  sealed trait State

  object State {
    def apply(state : String): State = state match {
      case "Hot" => Hot
      case "Cold" => Cold
      case "Stop" => Stop
      case _ => Stop
    }
  }
  case object Hot extends State

  case object Cold extends State

  case object Stop extends State
}
