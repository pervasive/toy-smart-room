package it.twin

import io.vertx.scala.core.Vertx
import it.home.env.HomeTwinVerticle

object HomeTwinApp extends App {
  val fakeBridge = new ThermostatSimulation(10, 0.1, 100).start()
  val verticle = new HomeTwinVerticle(8080, fakeBridge)
  Vertx.vertx().deployVerticle(verticle)
}
