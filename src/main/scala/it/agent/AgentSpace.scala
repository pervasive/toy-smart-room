package it.agent

import it.agent.medium.Medium

trait AgentSpace {
  type NAME
  type ENV
  type STATE
  type PLANNING

  type CONTEXT <: {
    def name : NAME
    def environment : ENV
    def state : STATE
  }

  type REF

  trait AgentPlanner {
    def goal(plan : PLANNING) : REF
  }
  type AGENT_BUILDER <: AgentPlanner

  type MEDIUM <: Medium[REF]

  def agent(context : CONTEXT) : AGENT_BUILDER

  def medium : MEDIUM
}