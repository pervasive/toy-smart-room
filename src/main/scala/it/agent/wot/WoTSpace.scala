package it.agent.wot

import it.agent.AgentSpace
import it.agent.medium.{ActorMedium, Message}
import akka.actor.{Actor, Props}
import akka.util.Timeout

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object WoTSpace extends AgentSpace {
  override type NAME = String
  override type ENV = DigitalEnv
  override type STATE = mutable.Map[String, Any]
  override type PLANNING = Seq[Plan[Context]]
  override type CONTEXT = Context
  case class Context(name : NAME, environment: ENV, state: STATE)
  override type REF = String
  override type AGENT_BUILDER = AgentPlanner
  override type MEDIUM = ActorMedium[REF]

  object dsl {
    val create = WoTSpace
    type Context = WoTSpace.CONTEXT
  }

  private case class AgentPlannerImpl(context: Context) extends AgentPlanner {
    def goal(goal : PLANNING) : REF = {
      medium.createActor(context.name, Props(classOf[AgentActor], context, goal))
      context.name
    }
  }
  def context(name : NAME, environment: ENV, state : STATE) : CONTEXT = {
    Context(name, environment, state)
  }

  override def agent(context: CONTEXT): AGENT_BUILDER = {
    AgentPlannerImpl(context)
  }

  def plan(plans : Plan[Context] *) : PLANNING = {
    Plan.combine(plans:_*)
  }
  override val medium: WoTSpace.MEDIUM = new ActorMedium[REF]("WoTSystem")

  class AgentActor(var agentContext : CONTEXT, var plan : PLANNING) extends Actor {
    import scala.concurrent.duration._
    var messagesStore : List[Message] = List()

    implicit val ec: ExecutionContext = context.dispatcher
    implicit val timeout: Timeout = 5 second

    def log(msg : String): Unit = {
      println(s"AGENT : ${agentContext.name} says : " + msg)
    }
    override def preStart(): Unit = {
      self ! "sensing"
    }

    def managePlans(): Unit = {
      val currentPlan = plan
      val filteredPlan = currentPlan.filter(executePlan)
      plan = filteredPlan
    }

    def removeInQueue(msg : Any): Unit = {
      val splittedAtMsg = messagesStore.partition(_.header == msg)
      messagesStore = splittedAtMsg._1.dropRight(1) ::: splittedAtMsg._2
    }

    def executePlan(plan : Plan[Context]) : Boolean = plan match {
      case Achieve(action, condition, onGoalSatisfied) =>
        if(condition(agentContext)) {
          onGoalSatisfied(agentContext)
          false
        } else {
          action(agentContext)
          true
        }
      case Maintain(action, condition, onGoalSatisfied) =>
        if(condition(agentContext)) {
          onGoalSatisfied(agentContext)
        } else {
          action(agentContext)
        }
        true
      case OnMessage(msgToFind, action) =>
        val message = messagesStore.find(_.header == msgToFind)
        message match {
          case Some(messageFound) =>
            println(messageFound)
            action(agentContext, messageFound.payload)
            removeInQueue(msgToFind)
          case _ =>
        }
        true
    }

    override def receive: Receive = {
      case "sensing" =>
        log("sensing...")
        agentContext.environment.sense.andThen({
          case Success(sensed) =>
            agentContext = agentContext.copy(environment = sensed)
            self ! "plan"
          case Failure(exception) => self ! "plan"
        })
      case "plan" =>
        log("planning...")
        managePlans()
        if(plan.nonEmpty) {
          context.system.scheduler.scheduleOnce(500 millis, self, "sensing")
        }
      case msg : Message => {
        messagesStore = msg :: messagesStore
      }
      case msg => log(s"not computed.. : ${msg}")
    }
  }
}