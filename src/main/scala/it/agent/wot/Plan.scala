package it.agent.wot

/**
  * generic plan, this is used to describe the behaviour and goals
  * of an agent
  * @tparam CONTEXT context type of agent
  */
sealed trait Plan[CONTEXT] { }

/**
  *
  * @param action
  * @param goal
  * @param onGoalSatisfied
  * @tparam CONTEXT context type of agent
  */
case class Achieve[CONTEXT](action : CONTEXT => Unit,
                   goal : CONTEXT => Boolean,
                   onGoalSatisfied : CONTEXT => Unit) extends Plan[CONTEXT]

/**
  *
  * @param action
  * @param goal
  * @param onGoalSatisfied
  * @tparam CONTEXT context type of agent
  */
case class Maintain[CONTEXT](action : CONTEXT => Unit,
                             goal : CONTEXT => Boolean,
                             onGoalSatisfied : CONTEXT => Unit) extends Plan[CONTEXT]

/**
  *
  * @param msg
  * @param action
  * @tparam CONTEXT context type of agent
  */
case class OnMessage[CONTEXT](msg : String,
                              action : (CONTEXT, Any) => Unit) extends Plan[CONTEXT]

object Plan {
  /**
    * some function utilities used to create plan in an "agile" ways
    */

  def combine[CONTEXT](plan: Plan[CONTEXT] *) : Seq[Plan[CONTEXT]] = List(plan:_*)

  def of[CONTEXT](plan : Plan[CONTEXT]) : Seq[Plan[CONTEXT]] = combine(plan)

  def achieve[CONTEXT](goal : CONTEXT => Boolean,
                       action : CONTEXT => Unit,
                       onGoalSatisfied : CONTEXT => Unit) : Achieve[CONTEXT] = {
    Achieve(action, goal, onGoalSatisfied)
  }
  def forever[CONTEXT](action : CONTEXT => Unit) : Maintain[CONTEXT] = Maintain(action, _ => false, _ => {})

  def maintain[CONTEXT](goal : CONTEXT => Boolean,
                        action : CONTEXT => Unit,
                        onGoalSatisfied : CONTEXT => Unit) : Maintain[CONTEXT] = {
    Maintain(action, goal,onGoalSatisfied)
  }
  def onMessage[CONTEXT](matchOn : String,
                         action : (CONTEXT, Any) => Unit) : OnMessage[CONTEXT] = OnMessage(matchOn, action)
}