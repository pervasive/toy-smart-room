package it.agent.wot

import scala.concurrent.Future

/**
  * something that could be sensed
  * @tparam R the representation of the sensed thing
  */
trait Perceivable[R] {
  def sense() : Future[R]
}
