package it.agent.wot

/**
  * a snapshot of environment, it is immutable concept.
  * when sensing the env, it return new snapshot.
  * it has a set of resource identified by a name
  */
trait DigitalEnv extends Perceivable[DigitalEnv]{
  def resource(name : String) : Option[Resource[_]]
}

/**
  * describe a resource used to collect some information
  * @tparam REP the representation of the resource (its concrete class).
  */
trait Resource[REP] extends Perceivable[REP]{
  def name : String
}


