package it.agent.medium

import akka.actor.{ActorRef, ActorSystem, Props}

/**
  * the message used to communicate to agent
 */
trait Message {
  def header : String
  def payload : Any
}

/**
  * it is the medium where agent are placed.
  * this concept can be used to communicate
  * with agent.
  * @tparam AGENT_REF the reference type used to select the agent
  */
trait Medium[AGENT_REF] {
  def sendMessageTo(msg : Message, ref : AGENT_REF) : Boolean
}

/**
  * a medium based on actor (akka)
  * @param systemName the akka system name
  * @tparam AGENT_REF the reference type used to select the agent
  */
class ActorMedium[AGENT_REF](systemName : String) extends Medium[AGENT_REF]{
  private val actorSystem = ActorSystem(systemName)
  private var agentNameToRef = Map.empty[AGENT_REF, ActorRef]

  override def sendMessageTo(msg: Message, agent: AGENT_REF): Boolean = {
    agentNameToRef.get(agent) match {
      case Some(ref) =>
        ref ! msg
        true
      case _ => false
    }
  }

  def createActor(agentName : AGENT_REF, props : Props): Unit = {
    val agent = actorSystem.actorOf(props)
    agentNameToRef += agentName -> agent
  }
}
